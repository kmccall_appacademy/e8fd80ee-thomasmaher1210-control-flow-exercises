# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ""
  str.chars.each {|let| let.downcase == let ? next : new_str += let}
  new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  half = str.length/2
  if str.length % 2 == 0
    return str[half-1] + str[half]
  else
    return str[half.floor]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count  = 0
  str.chars.each {|l| VOWELS.include?(l.downcase) ? count +=1 : next}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 0
  (1..num).each do |n|
    if n == 1
      factorial += n
    else
      factorial *= n
    end
  end
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return_str = ""
  arr.each.with_index do |el, i|
    if i == arr.length-1
      return_str += el
    else
      return_str += el + separator
    end
  end
  return_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_string = ""
  str.chars.each.with_index do |let, i|
    if i.even?
      weird_string += let.downcase
    else
      weird_string += let.upcase
    end
  end
  weird_string
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  return_str = []
  str.split(" ").each do |word|
    if word.length >= 5
      return_str << word.reverse
    else
      return_str << word
    end
  end
  return_str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |int|
    if (int % 3 == 0) && (int % 5 == 0)
      arr << "fizzbuzz"
    elsif int % 3 == 0
      arr << "fizz"
    elsif int % 5 == 0
      arr << "buzz"
    else
      arr << int
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2..num-1).none? {|n| num % n == 0}
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select {|n| num % n == 0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|n| prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
    if arr[0].odd? == arr[-1].odd?
      arr.select {|s| s.odd? != arr[0].odd?}.pop
    elsif arr[1].odd? == arr[-1].odd?
      return arr[0]
    else
      return arr[-1]
    end
end
